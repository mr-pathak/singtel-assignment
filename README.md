# Assignment Summary

## Source code (Java8)
I chose `composition over inheritance` for this assignment.

I used `TDD` methodolgy to implement most of the requirements mentioned in the assignment.

The class/package structure is as follows:-

- **animals.Animal** - Base class which encapsulates certain behaviours shown by all animals
- **animals.core.behaviour.*** - Package that contains behaviour of different animals. Ex:- CannotWalk, Walk, Fly, CannotFly etc.
- **animals.bird.*** - Package that contains all birds
- **animals.fish.*** - Package that contains all fishes
- **animals.insect.*** - Package that contains Butterfly

Scope for improvement:-

1. Use `spring` instead for dependency injection
2. This would also make it easy to implement Bonus question to support internationalization
3. And a `spring-boot` application with REST API

Sample REST API to access Animal data:-

1. **Get all animals** - GET /v1/animals
2. **Get specific animal by id** - GET /v1/animals/:id
3. **Get animals that can't fly** - GET /v1/animals?flybehaviour=cannotfly
4. **Get all birds that can fly** - GET /v1/animals/birds?flybehaviour=flyable
5. **Get all fishes** - GET /v1/animals/fishes
6. **Get fishes that are large** - GET /v1/animals/fishes?size=large


## MAVEN
I used Maven to manage my project.
Hence 

 - The source code can be found in **src/main/java** directory
 - The test can be found in **src/test/java** directory
 - To run tests `mvn test`

# Commits
Each commit is implements the task mentioned in the assignment.

# Time constraint
Due to time constraint, I :-

1. ignored null checks and argument validation
2. relaxed access modifies for fields
3. used lombok to use getters/setters
4. ignored refactoring of tests (may find some repetitative code)
5. chose simple project name

The time taken to complete this assignment is around `2 hours` (includes Readme and refactoring at the end).

