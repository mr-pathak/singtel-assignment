package com.miteshpathak;

import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;

import java.util.List;

public class Solution {
    public static void main( String[] args ) {
        List<Animal> animals = AnimalFactory.getAllAnimalsImplemented();

        System.out.println("Total animals = " + animals.size());

        printCountOfAnimalsThatCanFly(animals);
        printCountOfAnimalsThatCanWalk(animals);
        printCountOfAnimalsThatCanSwim(animals);
        printCountOfAnimalsThatCanSing(animals);
    }

    private static void printCountOfAnimalsThatCanFly(List<Animal> animals) {
        long count = animals.stream().filter(s -> s.getFlyBehaviour().canFly()).count();
        System.out.println("Number of animals that can fly = " + count);
    }

    private static void printCountOfAnimalsThatCanWalk(List<Animal> animals) {
        long count = animals.stream().filter(s -> s.getWalkBehaviour().canWalk()).count();
        System.out.println("Number of animals that can walk = " + count);
    }

    private static void printCountOfAnimalsThatCanSwim(List<Animal> animals) {
        long count = animals.stream().filter(s -> s.getSwimBehaviour().canSwim()).count();
        System.out.println("Number of animals that can swim = " + count);
    }

    private static void printCountOfAnimalsThatCanSing(List<Animal> animals) {
        long count = animals.stream().filter(s -> s.getSoundBehaviour().canSing()).count();
        System.out.println("Number of animals that can sing = " + count);
    }
}
