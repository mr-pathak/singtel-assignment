package com.miteshpathak.animals.insect;

import com.miteshpathak.animals.core.behaviour.fly.CannotFly;
import com.miteshpathak.animals.core.behaviour.fly.Flyable;

public class CaterPillar extends ButterFly {
    public CaterPillar() {
        this.flyBehaviour = new CannotFly();
    }

    public void transform() {
        this.flyBehaviour = new Flyable();
    }
}
