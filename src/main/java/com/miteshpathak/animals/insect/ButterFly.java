package com.miteshpathak.animals.insect;

import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.core.behaviour.fly.Flyable;
import com.miteshpathak.animals.core.behaviour.sound.CannotSing;
import com.miteshpathak.animals.core.behaviour.swim.Swimmer;
import com.miteshpathak.animals.core.behaviour.walk.DefaultWalkBehaviour;

public class ButterFly extends Animal {
    public ButterFly() {
        this.soundBehaviour = new CannotSing();
        this.swimBehaviour = new Swimmer();

        this.flyBehaviour = new Flyable();
        this.walkBehaviour = new DefaultWalkBehaviour();
    }
}
