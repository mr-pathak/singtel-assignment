package com.miteshpathak.animals;

import com.miteshpathak.animals.core.behaviour.fly.Flyable;
import com.miteshpathak.animals.core.behaviour.sound.CockADooSound;
import com.miteshpathak.animals.core.behaviour.sound.DefaultSound;
import com.miteshpathak.animals.core.behaviour.sound.MeowSound;
import com.miteshpathak.animals.core.behaviour.sound.WoofSound;
import com.miteshpathak.animals.core.behaviour.swim.CannotSwim;
import com.miteshpathak.animals.bird.*;
import com.miteshpathak.animals.bird.Bird;
import com.miteshpathak.animals.fish.ClownFish;
import com.miteshpathak.animals.fish.Dolphin;
import com.miteshpathak.animals.fish.Fish;
import com.miteshpathak.animals.fish.Shark;
import com.miteshpathak.animals.insect.ButterFly;
import com.miteshpathak.animals.insect.CaterPillar;

import java.util.Arrays;
import java.util.List;

public class AnimalFactory {
    public static Bird createDefaultBird() {
        return new Bird(new Flyable(), new DefaultSound(), new CannotSwim());
    }

    public static Duck createDuck() {
        return new Duck();
    }

    public static Chicken createChicken() {
        return new Chicken();
    }

    public static Rooster createRooster() {
        return new Rooster();
    }

    public static Parrot createParrotLivingNearDog() {
        return new Parrot(new WoofSound());
    }

    public static Parrot createParrotLivingNearCat() {
        return new Parrot(new MeowSound());
    }

    public static Parrot createParrotLivingNearRooster() {
        return new Parrot(new CockADooSound());
    }

    public static Fish createFish() {
        return new Fish();
    }

    public static Shark createShark() {
        return new Shark();
    }

    public static ClownFish createClownFish() {
        return new ClownFish();
    }

    public static Dolphin createDolphin() {
        return new Dolphin();
    }

    public static ButterFly createButterFly() {
        return new ButterFly();
    }

    public static CaterPillar createCaterPillar() {
        return new CaterPillar();
    }

    public static List<Animal> getAllAnimalsImplemented() {
        return Arrays.asList(
                createDefaultBird(),
                createDuck(),
                createChicken(),
                createRooster(),
                createParrotLivingNearDog(),
                createFish(),
                createShark(),
                createClownFish(),
                createDolphin(),
                createButterFly(),
                createCaterPillar()
        );
    }
}
