package com.miteshpathak.animals;

import com.miteshpathak.animals.core.behaviour.fly.FlyBehaviour;
import com.miteshpathak.animals.core.behaviour.sound.SoundBehaviour;
import com.miteshpathak.animals.core.behaviour.swim.SwimBehaviour;
import com.miteshpathak.animals.core.behaviour.walk.DefaultWalkBehaviour;
import com.miteshpathak.animals.core.behaviour.walk.WalkBehaviour;
import lombok.Getter;

public abstract class Animal {
    @Getter
    protected WalkBehaviour walkBehaviour = new DefaultWalkBehaviour();

    @Getter
    protected FlyBehaviour flyBehaviour;

    @Getter
    protected SoundBehaviour soundBehaviour;

    @Getter
    protected SwimBehaviour swimBehaviour;

    public void walk() {
        walkBehaviour.walkNow();
    }

    public void fly() {
        flyBehaviour.flyNow();
    }

    public void sing() {
        soundBehaviour.makeSound();
    }

    public void swim() {
        swimBehaviour.swimNow();
    }
}
