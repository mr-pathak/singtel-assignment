package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.core.features.Color;
import com.miteshpathak.animals.core.features.Size;

public class ClownFish extends Fish {
    public ClownFish() {
        this.color = Color.ORANGE;
        this.size = Size.SMALL;
    }

    public void joke() {
        System.out.println("This tastes funny.");
    }
}
