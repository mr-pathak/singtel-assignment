package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.core.features.Color;
import com.miteshpathak.animals.core.features.Size;

public class Shark extends Fish {
    public Shark() {
        this.color = Color.GREY;
        this.size = Size.LARGE;
    }

    public void eat(Fish otherFish) {
        if (otherFish != null) {
            System.out.println("I am eating " + otherFish.getType());
        }
    }
}
