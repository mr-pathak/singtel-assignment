package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.core.behaviour.fly.CannotFly;
import com.miteshpathak.animals.core.behaviour.sound.WhistleSound;
import com.miteshpathak.animals.core.behaviour.swim.Swimmer;
import com.miteshpathak.animals.core.behaviour.walk.CannotWalk;

public class Dolphin extends Animal {
    public Dolphin() {
        this.flyBehaviour = new CannotFly();
        this.soundBehaviour = new WhistleSound();
        this.walkBehaviour = new CannotWalk();

        this.swimBehaviour = new Swimmer();
    }
}
