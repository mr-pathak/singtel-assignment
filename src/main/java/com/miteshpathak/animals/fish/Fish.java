package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.core.behaviour.fly.CannotFly;
import com.miteshpathak.animals.core.behaviour.sound.CannotSing;
import com.miteshpathak.animals.core.behaviour.swim.Swimmer;
import com.miteshpathak.animals.core.behaviour.walk.CannotWalk;
import com.miteshpathak.animals.core.features.Color;
import com.miteshpathak.animals.core.features.Size;
import lombok.Getter;

public class Fish extends Animal {
    @Getter
    protected Color color;

    @Getter
    protected Size size;

    public Fish() {
        this.flyBehaviour = new CannotFly();
        this.soundBehaviour = new CannotSing();
        this.walkBehaviour = new CannotWalk();

        this.swimBehaviour = new Swimmer();
    }

    public String getType() {
        return this.getClass().getSimpleName();
    }
}
