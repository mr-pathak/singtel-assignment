package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.core.behaviour.fly.Flyable;
import com.miteshpathak.animals.core.behaviour.sound.QuackSound;
import com.miteshpathak.animals.core.behaviour.swim.Swimmer;

public class Duck extends Bird {
    public Duck() {
        super(new Flyable(), new QuackSound(), new Swimmer());
    }
}
