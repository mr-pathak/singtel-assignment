package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.core.behaviour.fly.Flyable;
import com.miteshpathak.animals.core.behaviour.sound.SoundBehaviour;
import com.miteshpathak.animals.core.behaviour.swim.CannotSwim;

public class Parrot extends Bird {
    public Parrot(SoundBehaviour soundToImitate) {
        super(new Flyable(), soundToImitate, new CannotSwim());
    }
}
