package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.core.behaviour.fly.CannotFly;
import com.miteshpathak.animals.core.behaviour.sound.CluckSound;
import com.miteshpathak.animals.core.behaviour.swim.CannotSwim;

public class Chicken extends Bird {
    public Chicken() {
        super(new CannotFly(), new CluckSound(), new CannotSwim());
    }
}
