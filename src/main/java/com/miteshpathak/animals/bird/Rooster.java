package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.core.behaviour.sound.CockADooSound;

public class Rooster extends Chicken {
    public Rooster() {
        this.soundBehaviour = new CockADooSound();
    }
}
