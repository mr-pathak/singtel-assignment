package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.core.behaviour.fly.FlyBehaviour;
import com.miteshpathak.animals.core.behaviour.sound.SoundBehaviour;
import com.miteshpathak.animals.core.behaviour.swim.SwimBehaviour;

public class Bird extends Animal {
    public Bird(FlyBehaviour flyBehaviour, SoundBehaviour soundBehaviour, SwimBehaviour swimBehaviour) {
        this.flyBehaviour = flyBehaviour;
        this.soundBehaviour = soundBehaviour;
        this.swimBehaviour = swimBehaviour;
    }
}
