package com.miteshpathak.animals.core.features;

public enum  Size {
    LARGE, SMALL, MEDIUM
}
