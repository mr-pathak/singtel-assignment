package com.miteshpathak.animals.core.features;

public enum Color {
    GREY, WHITE, BLACK, ORANGE;

    public boolean isColorFull() {
        switch (this) {
            case GREY:
            case BLACK:
            case WHITE:
                return false;
        }

        return true;
    }
}
