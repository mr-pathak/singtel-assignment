package com.miteshpathak.animals.core.behaviour.walk;

public class CannotWalk implements WalkBehaviour {
    public boolean canWalk() {
        return false;
    }

    public void walkNow() {
        System.out.println("I can't walk");
    }
}
