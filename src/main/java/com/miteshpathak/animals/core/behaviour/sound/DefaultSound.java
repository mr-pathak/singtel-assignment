package com.miteshpathak.animals.core.behaviour.sound;

public class DefaultSound implements SoundBehaviour {

    public boolean canSing() {
        return true;
    }

    public void makeSound() {
        System.out.println("I am singing");
    }
}
