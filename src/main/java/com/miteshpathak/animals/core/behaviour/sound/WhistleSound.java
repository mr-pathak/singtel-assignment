package com.miteshpathak.animals.core.behaviour.sound;

public class WhistleSound extends DefaultSound {
    @Override
    public void makeSound() {
        System.out.println("Whistles");
    }
}
