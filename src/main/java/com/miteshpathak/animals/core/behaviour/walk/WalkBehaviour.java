package com.miteshpathak.animals.core.behaviour.walk;

public interface WalkBehaviour {
    boolean canWalk();
    void walkNow();
}

