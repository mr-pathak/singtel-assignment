package com.miteshpathak.animals.core.behaviour.sound;

public class CluckSound extends DefaultSound {
    @Override
    public void makeSound() {
        System.out.println("Cluck, Cluck");
    }
}
