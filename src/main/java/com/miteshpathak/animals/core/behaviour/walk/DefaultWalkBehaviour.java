package com.miteshpathak.animals.core.behaviour.walk;

public class DefaultWalkBehaviour implements WalkBehaviour {
    public boolean canWalk() {
        return true;
    }

    public void walkNow() {
        System.out.println("I am walking");
    }
}
