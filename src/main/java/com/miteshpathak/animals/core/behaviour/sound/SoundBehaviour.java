package com.miteshpathak.animals.core.behaviour.sound;

public interface SoundBehaviour {
    boolean canSing();
    void makeSound();
}
