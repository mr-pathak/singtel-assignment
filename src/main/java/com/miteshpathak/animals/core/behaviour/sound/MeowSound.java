package com.miteshpathak.animals.core.behaviour.sound;

public class MeowSound extends DefaultSound {

    @Override
    public void makeSound() {
        System.out.println("Meow");
    }
}
