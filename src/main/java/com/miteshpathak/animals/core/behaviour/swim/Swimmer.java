package com.miteshpathak.animals.core.behaviour.swim;

public class Swimmer implements SwimBehaviour{
    public boolean canSwim() {
        return true;
    }

    public void swimNow() {
        System.out.println("I am swimming");
    }
}
