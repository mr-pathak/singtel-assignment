package com.miteshpathak.animals.core.behaviour.sound;

public class QuackSound extends DefaultSound {

    @Override
    public void makeSound() {
        System.out.println("Quack, Quack");
    }
}
