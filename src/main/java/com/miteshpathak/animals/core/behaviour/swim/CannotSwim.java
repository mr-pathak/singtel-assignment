package com.miteshpathak.animals.core.behaviour.swim;

public class CannotSwim implements SwimBehaviour {
    public boolean canSwim() {
        return false;
    }

    public void swimNow() {
        System.out.println("I can't swim");
    }
}
