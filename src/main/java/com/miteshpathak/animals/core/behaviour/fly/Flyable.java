package com.miteshpathak.animals.core.behaviour.fly;

public class Flyable implements FlyBehaviour {
    public boolean canFly() {
        return true;
    }

    public void flyNow() {
        System.out.println("I am flying");
    }
}
