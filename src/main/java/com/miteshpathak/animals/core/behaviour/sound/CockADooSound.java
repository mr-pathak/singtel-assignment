package com.miteshpathak.animals.core.behaviour.sound;

public class CockADooSound extends DefaultSound {
    @Override
    public void makeSound() {
        System.out.println("Cock-a-doodle-doo");
    }
}
