package com.miteshpathak.animals.core.behaviour.fly;

public class CannotFly implements FlyBehaviour {
    public boolean canFly() {
        return false;
    }

    public void flyNow() {
        System.out.println("I can't fly");
    }
}
