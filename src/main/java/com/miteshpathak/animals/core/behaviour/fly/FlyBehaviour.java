package com.miteshpathak.animals.core.behaviour.fly;

public interface FlyBehaviour {
    boolean canFly();
    void flyNow();
}
