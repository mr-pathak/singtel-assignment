package com.miteshpathak.animals.core.behaviour.swim;

public interface SwimBehaviour {
    boolean canSwim();
    void swimNow();
}
