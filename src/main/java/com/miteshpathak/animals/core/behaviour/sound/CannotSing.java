package com.miteshpathak.animals.core.behaviour.sound;

public class CannotSing implements SoundBehaviour {
    public boolean canSing() {
        return false;
    }

    public void makeSound() {
        System.out.println("I can't sing");
    }
}
