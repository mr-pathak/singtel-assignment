package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ParrotTest extends AbstractAnimalTest {
    @Test
    public void testParrotNearRoosterCanCockADoo() {
        Parrot parrot = AnimalFactory.createParrotLivingNearRooster();

        parrot.sing();
        assertPrintedOutputEquals("Cock-a-doodle-doo");
    }

    @Test
    public void testParrotNearDogCanWoofWoof() {
        Parrot parrot = AnimalFactory.createParrotLivingNearDog();

        parrot.sing();
        assertPrintedOutputEquals("Woof, woof");
    }

    @Test
    public void testParrotNearCatCanMeow() {
        Parrot parrot = AnimalFactory.createParrotLivingNearCat();

        parrot.sing();
        assertPrintedOutputEquals("Meow");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createParrotLivingNearDog();
    }
}
