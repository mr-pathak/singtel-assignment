package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BirdTest extends AbstractAnimalTest {
    @Test
    public void testFlyBehaviour() {
        Bird bird = AnimalFactory.createDefaultBird();
        assertTrue(bird.getFlyBehaviour().canFly());

        bird.fly();
        assertPrintedOutputEquals("I am flying");
    }

    @Test
    public void testWalkBehaviour() {
        Bird bird = AnimalFactory.createDefaultBird();
        assertTrue(bird.getWalkBehaviour().canWalk());

        bird.walk();
        assertPrintedOutputEquals("I am walking");
    }

    @Test
    public void testSingBehaviour() {
        Bird bird = AnimalFactory.createDefaultBird();

        bird.sing();
        assertPrintedOutputEquals("I am singing");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createDefaultBird();
    }
}
