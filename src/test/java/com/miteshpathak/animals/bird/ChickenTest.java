package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class ChickenTest extends AbstractAnimalTest {
    @Test
    public void testChickenCanCluck() {
        Chicken chicken = AnimalFactory.createChicken();
        chicken.sing();

        assertPrintedOutputEquals("Cluck, Cluck");
    }

    @Test
    public void testChickenCannotSwim() {
        Chicken chicken = AnimalFactory.createChicken();

        assertFalse(chicken.getSwimBehaviour().canSwim());

        chicken.swim();
        assertPrintedOutputEquals("I can't swim");
    }

    @Test
    public void testChickenCannotFly() {
        Chicken chicken = AnimalFactory.createChicken();
        assertFalse(chicken.getFlyBehaviour().canFly());

        chicken.fly();
        assertPrintedOutputEquals("I can't fly");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createChicken();
    }
}
