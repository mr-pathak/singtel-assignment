package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class RoosterTest extends AbstractAnimalTest {
    @Test
    public void testRoosterCanCockADoo() {
        Rooster rooster = AnimalFactory.createRooster();
        rooster.sing();

        assertPrintedOutputEquals("Cock-a-doodle-doo");
    }

    @Test
    public void testRoosterIsAChicken() {
        Rooster rooster = AnimalFactory.createRooster();

        assertTrue(rooster instanceof Chicken);
    }

    protected Animal animalInAction() {
        return AnimalFactory.createRooster();
    }
}
