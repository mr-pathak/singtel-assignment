package com.miteshpathak.animals.bird;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.*;

public class DuckTest extends AbstractAnimalTest {
    @Test
    public void testDuckCanQuack() {
        Duck duck = AnimalFactory.createDuck();
        duck.sing();

        assertPrintedOutputEquals("Quack, Quack");
    }

    @Test
    public void testDuckCanSwim() {
        Duck duck = AnimalFactory.createDuck();

        assertTrue(duck.getSwimBehaviour().canSwim());

        duck.swim();
        assertPrintedOutputEquals("I am swimming");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createDuck();
    }
}
