package com.miteshpathak.animals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public abstract class AbstractAnimalTest {
    private final ByteArrayOutputStream recordingStream = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void recordOutputStream() {
        System.setOut(new PrintStream(recordingStream));
    }

    @After
    public void restoreOutputStream() {
        System.setOut(originalOut);
    }

    protected void assertPrintedOutputEquals(String expectedOutput) {
        assertEquals(withNewLine(expectedOutput), recordingStream.toString());
    }

    private String withNewLine(String inp) {
        if (inp == null) return null;
        return inp + System.lineSeparator();
    }

    protected abstract Animal animalInAction();

    @Test
    public void testProperties() {
        Animal animal = animalInAction();

        assertNotNull(animal.getWalkBehaviour());
        assertNotNull(animal.getFlyBehaviour());
        assertNotNull(animal.getSwimBehaviour());
        assertNotNull(animal.getSoundBehaviour());
    }
}
