package com.miteshpathak.animals.insect;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CaterPillarTest extends AbstractAnimalTest {
    @Test
    public void testCaterPillarCannotActuallyFly() {
        CaterPillar caterPillar = AnimalFactory.createCaterPillar();

        assertFalse(caterPillar.getFlyBehaviour().canFly());
    }

    @Test
    public void testCaterPillarIsAButterFly() {
        CaterPillar caterPillar = AnimalFactory.createCaterPillar();

        assertTrue(caterPillar instanceof ButterFly);
    }

    @Test
    public void testCaterPillarTransformsToButterFly() {
        CaterPillar caterPillar = AnimalFactory.createCaterPillar();
        caterPillar.transform();

        assertTrue(caterPillar.getFlyBehaviour().canFly());
    }

    protected Animal animalInAction() {
        return AnimalFactory.createCaterPillar();
    }
}
