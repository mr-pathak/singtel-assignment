package com.miteshpathak.animals.insect;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ButterFlyTest extends AbstractAnimalTest {
    @Test
    public void testButterFlyCanActuallyFly() {
        ButterFly insect = AnimalFactory.createButterFly();

        assertTrue(insect.getFlyBehaviour().canFly());
    }

    @Test
    public void testButterFlyCannotMakeSound() {
        ButterFly insect = AnimalFactory.createButterFly();

        assertFalse(insect.getSoundBehaviour().canSing());
    }

    protected Animal animalInAction() {
        return AnimalFactory.createButterFly();
    }
}
