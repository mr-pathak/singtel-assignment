package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import com.miteshpathak.animals.core.features.Size;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ClownFishTest extends AbstractAnimalTest {
    @Test
    public void testClownFishIsAFish() {
        ClownFish clownFish = AnimalFactory.createClownFish();
        assertTrue(clownFish instanceof Fish);
    }

    @Test
    public void testClownFishIsColorFulAndSmall() {
        Fish fish = AnimalFactory.createClownFish();

        assertTrue(fish.getColor().isColorFull());
        assertEquals(fish.getSize(), Size.SMALL);
    }

    @Test
    public void testClownFishMakesJoke() {
        ClownFish clownFish = AnimalFactory.createClownFish();
        clownFish.joke();

        assertPrintedOutputEquals("This tastes funny.");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createClownFish();
    }
}
