package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FishTest extends AbstractAnimalTest {
    @Test
    public void testFishCannotFly() {
        Fish fish = AnimalFactory.createFish();
        assertFalse(fish.getFlyBehaviour().canFly());
    }

    @Test
    public void testFishCannotWalk() {
        Fish fish = AnimalFactory.createFish();
        assertFalse(fish.getWalkBehaviour().canWalk());
    }

    @Test
    public void testFishCannotSing() {
        Fish fish = AnimalFactory.createFish();
        assertFalse(fish.getSoundBehaviour().canSing());
    }

    @Test
    public void testFishCanSwim() {
        Fish fish = AnimalFactory.createFish();
        assertTrue(fish.getSwimBehaviour().canSwim());
    }

    protected Animal animalInAction() {
        return AnimalFactory.createFish();
    }
}
