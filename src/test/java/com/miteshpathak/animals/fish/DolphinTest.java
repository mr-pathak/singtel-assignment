package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.AnimalFactory;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DolphinTest extends AbstractAnimalTest {
    @Test
    public void testDolphinCannotFly() {
        Dolphin dolphin = AnimalFactory.createDolphin();
        assertFalse(dolphin.getFlyBehaviour().canFly());
    }

    @Test
    public void testDolphinCannotWalk() {
        Dolphin dolphin = AnimalFactory.createDolphin();
        assertFalse(dolphin.getWalkBehaviour().canWalk());
    }

    @Test
    public void testDolphinCanWhistle() {
        Dolphin dolphin = AnimalFactory.createDolphin();
        assertTrue(dolphin.getSoundBehaviour().canSing());

        dolphin.sing();
        assertPrintedOutputEquals("Whistles");
    }

    @Test
    public void testDolphinCanSwim() {
        Dolphin dolphin = AnimalFactory.createDolphin();
        assertTrue(dolphin.getSwimBehaviour().canSwim());
    }

    protected Animal animalInAction() {
        return AnimalFactory.createDolphin();
    }
}
