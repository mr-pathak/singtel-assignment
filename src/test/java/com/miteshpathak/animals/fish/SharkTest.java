package com.miteshpathak.animals.fish;

import com.miteshpathak.animals.AbstractAnimalTest;
import com.miteshpathak.animals.AnimalFactory;
import com.miteshpathak.animals.Animal;
import com.miteshpathak.animals.core.features.Color;
import com.miteshpathak.animals.core.features.Size;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SharkTest extends AbstractAnimalTest {
    @Test
    public void testSharkIsAFish() {
        Shark shark = AnimalFactory.createShark();
        assertTrue(shark instanceof Fish);
    }

    @Test
    public void testSharkIsGreyAndLarge() {
        Fish fish = AnimalFactory.createShark();

        assertEquals(fish.getColor(), Color.GREY);
        assertEquals(fish.getSize(), Size.LARGE);
    }

    @Test
    public void testSharkCanEatOtherFish() {
        Shark shark = AnimalFactory.createShark();
        shark.eat(AnimalFactory.createFish());

        assertPrintedOutputEquals("I am eating Fish");
    }

    protected Animal animalInAction() {
        return AnimalFactory.createShark();
    }
}
